Docker image repository. 

All images here are a base for different microservices.

Python Base:

[![](https://images.microbadger.com/badges/image/youpooly/python3-base.svg)](https://microbadger.com/images/youpooly/python3-base "Get your own image badge on microbadger.com")

[![](https://images.microbadger.com/badges/version/youpooly/python3-base.svg)](https://microbadger.com/images/youpooly/python3-base "Get your own version badge on microbadger.com")

Cairo/Graphics enabled Image:
